# Chartmuseum ansible role

## Role settings

Must set one of these two values as they are both False by default. Depends on if you want chartmuseum on a docker host or inside a k8s cluster. The Docker host must be setup for docker use, not podman.

* ``chartmuseum_install_docker`` - Install as docker container using [``docker_container``](https://docs.ansible.com/ansible/latest/modules/docker_container_module.html) ansible module.
* ``chartmuseum_install_k8s`` - Install Helm chart using helm binary. Requires [Helm](https://gitlab.com/stemid-ansible/roles/helm.git) obviously.

Can also set some of these values.

* ``chartmuseum_helm_repo`` - Source repo name where we find the helm chart for Chartmuseum. Defaults to ``helmhub-stable`` because that's what I use in my [kubeadm-cluster playbooks](https://gitlab.com/stemid-ansible/playbooks/kubeadm-cluster.git). *This repo must obviously exist, the role does not install the repo at this moment.*
* ``chartmuseum_helm_chart`` - Defaults to ``chartmuseum``.
* ``chartmuseum_override_values`` - Only overrides specific values, based on ``chartmuseum_default_values`` in ``defaults/main.yml``.
* ``chartmuseum_helm_values`` - Overrides all helm values.

